[console_scripts]
limnoria = supybot.scripts.limnoria:main
limnoria-adduser = supybot.scripts.limnoria_adduser:main
limnoria-botchk = supybot.scripts.limnoria_botchk:main
limnoria-plugin-create = supybot.scripts.limnoria_plugin_create:main
limnoria-plugin-doc = supybot.scripts.limnoria_plugin_doc:main
limnoria-reset-password = supybot.scripts.limnoria_reset_password:main
limnoria-test = supybot.scripts.limnoria_test:main
limnoria-wizard = supybot.scripts.limnoria_wizard:main
supybot = supybot.scripts.limnoria:main
supybot-adduser = supybot.scripts.limnoria_adduser:main
supybot-botchk = supybot.scripts.limnoria_botchk:main
supybot-plugin-create = supybot.scripts.limnoria_plugin_create:main
supybot-plugin-doc = supybot.scripts.limnoria_plugin_doc:main
supybot-reset-password = supybot.scripts.limnoria_reset_password:main
supybot-test = supybot.scripts.limnoria_test:main
supybot-wizard = supybot.scripts.limnoria_wizard:main
